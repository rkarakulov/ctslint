import * as ts from 'typescript';
import * as Lint from 'tslint';
import { ExtendedMetadata } from './utils/ExtendedMetadata';
export declare const FAILURE_CLASS_STRING: string;
export declare const FAILURE_FILE_STRING: string;
export declare const FAILURE_BLOCK_STRING: string;
export declare class Rule extends Lint.Rules.AbstractRule {
    static metadata: ExtendedMetadata;
    apply(sourceFile: ts.SourceFile): Lint.RuleFailure[];
}
