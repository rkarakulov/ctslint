"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
exports.FAILURE_MIN_STRING = 'Too short; difficult to understand its purpose without context';
exports.FAILURE_MAX_STRING = 'Too long; difficult to read and potentially less maintainable';
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new IdLengthRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'id-length',
        type: 'maintainability',
        description: 'This rule enforces a minimum and/or maximum identifier length convention.',
        options: null,
        optionsDescription: '',
        typescriptOnly: true,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Important',
        level: 'Opportunity for Excellence',
        group: 'Correctness',
        recommendation: 'true,',
        commonWeaknessEnumeration: '',
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var IdLengthRuleWalker = (function (_super) {
    __extends(IdLengthRuleWalker, _super);
    function IdLengthRuleWalker(sourceFile, options) {
        var _this = _super.call(this, sourceFile, options) || this;
        _this.min = 2;
        _this.max = Infinity;
        _this.exceptions = [];
        _this.parseOptions();
        return _this;
    }
    IdLengthRuleWalker.prototype.visitIdentifier = function (node) {
        this.checkAndReport(node);
        _super.prototype.visitIdentifier.call(this, node);
    };
    IdLengthRuleWalker.prototype.checkAndReport = function (node) {
        var text = node.text;
        if (this.exceptions.indexOf(text) === -1) {
            if (text.length < this.min) {
                return this.addFailureAt(node.getStart(), node.getWidth(), exports.FAILURE_MIN_STRING + ': ' + text);
            }
            if (text.length > this.max) {
                return this.addFailureAt(node.getStart(), node.getWidth(), exports.FAILURE_MAX_STRING + ': ' + text);
            }
        }
    };
    IdLengthRuleWalker.prototype.parseOptions = function () {
        var _this = this;
        this.getOptions().forEach(function (opt) {
            if (typeof opt === 'boolean') {
                return;
            }
            if (typeof opt === 'number') {
                _this.min = opt;
                return;
            }
            if (Array.isArray(opt)) {
                _this.exceptions = opt;
                return;
            }
            if (typeof opt === 'object') {
                _this.min = typeof opt.min === 'number' ? opt.min : _this.min;
                _this.max = typeof opt.max === 'number' ? opt.max : _this.max;
                _this.exceptions = opt.exceptions || _this.exceptions;
                return;
            }
        });
    };
    return IdLengthRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
//# sourceMappingURL=idLengthRule.js.map