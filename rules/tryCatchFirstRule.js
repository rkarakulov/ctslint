"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
exports.FAILURE_STRING = 'Try-catch blocks must be at the top of the scope';
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new TryCatchFirstRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'try-catch-first',
        type: 'maintainability',
        description: 'Try-catch blocks must be first within the scope. ' +
            'Try-catch blocks are transactions and should leave your program in a consistent state, no matter what happens in the try.',
        options: null,
        optionsDescription: '',
        typescriptOnly: true,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Important',
        level: 'Opportunity for Excellence',
        group: 'Correctness',
        recommendation: 'true,',
        commonWeaknessEnumeration: '',
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var TryCatchFirstRuleWalker = (function (_super) {
    __extends(TryCatchFirstRuleWalker, _super);
    function TryCatchFirstRuleWalker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.scopeKinds = [
            ts.SyntaxKind.FunctionDeclaration,
            ts.SyntaxKind.ArrowFunction,
            ts.SyntaxKind.SourceFile,
            ts.SyntaxKind.MethodDeclaration,
            ts.SyntaxKind.GetAccessor,
            ts.SyntaxKind.SetAccessor,
        ];
        return _this;
    }
    TryCatchFirstRuleWalker.prototype.visitTryStatement = function (node) {
        this.checkAndReport(node);
        _super.prototype.visitTryStatement.call(this, node);
    };
    TryCatchFirstRuleWalker.prototype.checkAndReport = function (node) {
        var block = node.parent;
        var parent = block.parent;
        var isFirst = this.scopeKinds.indexOf((parent || block).kind) !== -1;
        if (!isFirst) {
            var failureMessage = this.makeFailureMessage();
            this.addFailureAt(node.getStart(), node.getWidth(), failureMessage);
        }
    };
    TryCatchFirstRuleWalker.prototype.makeFailureMessage = function () {
        return exports.FAILURE_STRING;
    };
    return TryCatchFirstRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
//# sourceMappingURL=tryCatchFirstRule.js.map