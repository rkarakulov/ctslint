"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
exports.FAILURE_STRING = 'Flag (boolean) arguments are not allowed: ';
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoFlagArgsRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'no-flag-args',
        type: 'maintainability',
        description: 'Passing a boolean into a function is a truly terrible practice. ' +
            'It immediately complicates the signature of the method, loudly proclaiming that this function does more than one thing.',
        options: null,
        optionsDescription: '',
        typescriptOnly: true,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Important',
        level: 'Opportunity for Excellence',
        group: 'Correctness',
        recommendation: 'true,',
        commonWeaknessEnumeration: '',
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var NoFlagArgsRuleWalker = (function (_super) {
    __extends(NoFlagArgsRuleWalker, _super);
    function NoFlagArgsRuleWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoFlagArgsRuleWalker.prototype.visitParameterDeclaration = function (node) {
        if (this.isBooleanParameter(node) && !this.belongsToSetAssessor(node)) {
            var failureMessage = this.makeFailureMessage(node, exports.FAILURE_STRING);
            this.addFailureAt(node.getStart(), node.getWidth(), failureMessage);
        }
        _super.prototype.visitParameterDeclaration.call(this, node);
    };
    NoFlagArgsRuleWalker.prototype.isBooleanParameter = function (node) {
        var type = node.type;
        return type && type.kind === ts.SyntaxKind.BooleanKeyword;
    };
    NoFlagArgsRuleWalker.prototype.belongsToSetAssessor = function (node) {
        var parent = node.parent;
        return parent && parent.kind === ts.SyntaxKind.SetAccessor;
    };
    NoFlagArgsRuleWalker.prototype.makeFailureMessage = function (node, failureString) {
        var paramName = node.name.getText();
        var pascalCaseParamName = this.toPascalCase(paramName);
        var functionName = node.parent.name && node.parent.name.getText();
        var recommendation = functionName
            ? '\nSplit the function into two, such as ' +
                (functionName + "When" + pascalCaseParamName) +
                ' and ' +
                (functionName + "WhenNot" + pascalCaseParamName + ".")
            : '\nSplit the function into two.';
        return failureString + paramName + recommendation;
    };
    NoFlagArgsRuleWalker.prototype.toPascalCase = function (str) {
        if (typeof str !== 'string' || str.length === 0) {
            return str;
        }
        return str[0].toUpperCase() + str.slice(1);
    };
    return NoFlagArgsRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
//# sourceMappingURL=noFlagArgsRule.js.map