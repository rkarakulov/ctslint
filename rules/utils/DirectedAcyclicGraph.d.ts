export declare class DirectedAcyclicGraph {
    private numVertices;
    constructor(numVertices: number);
    private adj;
    private inDegree;
    alltopologicalSort(): number[][];
    private alltopologicalSortUtil(res, visited);
    addEdge(src: number, dest: number): void;
}
