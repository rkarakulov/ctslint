"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DirectedAcyclicGraph = (function () {
    function DirectedAcyclicGraph(numVertices) {
        this.adj = [];
        this.inDegree = [];
        this.numVertices = numVertices;
        for (var curr = 0; curr < numVertices; curr = curr + 1) {
            this.inDegree.push(0);
            this.adj.push([]);
        }
    }
    DirectedAcyclicGraph.prototype.alltopologicalSort = function () {
        var numVertices = this.numVertices;
        var visited = [];
        for (var curr = 0; curr < numVertices; curr = curr + 1) {
            visited.push(false);
        }
        var res = [];
        return this.alltopologicalSortUtil(res, visited);
    };
    DirectedAcyclicGraph.prototype.alltopologicalSortUtil = function (res, visited) {
        var _a = this, numVertices = _a.numVertices, inDegree = _a.inDegree, adj = _a.adj;
        var flag = false;
        var allSorts = [];
        var floor = inDegree.reduce(function (result, val, index) {
            if (visited[index] === false) {
                return Math.min(result, val);
            }
            return result;
        }, Infinity);
        for (var vertex = 0; vertex < numVertices; vertex++) {
            if (inDegree[vertex] === floor && !visited[vertex]) {
                var adjIndex = void 0;
                for (adjIndex = 0; adjIndex < adj[vertex].length; adjIndex++) {
                    var jv = adj[vertex][adjIndex];
                    inDegree[jv]--;
                }
                visited[vertex] = true;
                allSorts.push.apply(allSorts, this.alltopologicalSortUtil(res.concat(vertex), visited));
                visited[vertex] = false;
                for (adjIndex = 0; adjIndex < adj[vertex].length; adjIndex++) {
                    var jv = adj[vertex][adjIndex];
                    inDegree[jv]++;
                }
                flag = true;
            }
        }
        if (!flag) {
            allSorts.push(res);
        }
        return allSorts;
    };
    DirectedAcyclicGraph.prototype.addEdge = function (src, dest) {
        this.adj[src].push(dest);
        this.inDegree[dest]++;
    };
    return DirectedAcyclicGraph;
}());
exports.DirectedAcyclicGraph = DirectedAcyclicGraph;
//# sourceMappingURL=DirectedAcyclicGraph.js.map