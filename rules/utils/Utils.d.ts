import * as ts from 'typescript';
export declare namespace Utils {
    function exists<T extends ts.Node>(list: ts.NodeArray<T>, predicate: (t: T) => boolean): boolean;
    function contains<T extends ts.Node>(list: ts.NodeArray<T>, element: T): boolean;
    function removeAll<T extends ts.Node>(source: ts.NodeArray<T>, elementsToRemove: ts.NodeArray<T>): T[];
    function remove<T extends ts.Node>(source: ts.NodeArray<T>, elementToRemove: T): T[];
    function trimTo(source: string, maxLength: number): string;
    function arraysShallowEqual(arr1: any[], arr2: any[]): boolean;
}
