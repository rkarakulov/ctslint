import * as ts from 'typescript';
import * as Lint from 'tslint';
import { ExtendedMetadata } from './utils/ExtendedMetadata';
export declare class Rule extends Lint.Rules.AbstractRule {
    static metadata: ExtendedMetadata;
    static FAILURE_STRING(feature: MethodFeature): string;
    apply(sourceFile: ts.SourceFile): Lint.RuleFailure[];
}
export declare class MethodFeature {
    private data;
    constructor(data: {
        classNode: ts.ClassDeclaration;
        methodNode: ts.MethodDeclaration;
        otherClassName: string;
        thisClassAccesses: number;
        otherClassAccesses: number;
    });
    readonly className: string;
    readonly classNode: ts.ClassDeclaration;
    readonly methodName: string;
    readonly methodNode: ts.MethodDeclaration;
    featureEnvy(): number;
    readonly otherClassName: string;
}
