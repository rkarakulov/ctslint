"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoForeachPushRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'no-for-each-push',
        type: 'maintainability',
        description: 'Enforce using Array.prototype.map instead of Array.prototype.forEach and Array.prototype.push.',
        options: null,
        optionsDescription: '',
        typescriptOnly: true,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Important',
        level: 'Opportunity for Excellence',
        group: 'Correctness',
        recommendation: 'true,',
        commonWeaknessEnumeration: '',
    };
    Rule.FAILURE_STRING = 'Do not use Array.prototype.push inside of Array.prototype.forEach. ' +
        'Use Array.prototype.map instead to replace both.';
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var NoForeachPushRuleWalker = (function (_super) {
    __extends(NoForeachPushRuleWalker, _super);
    function NoForeachPushRuleWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoForeachPushRuleWalker.prototype.visitPropertyAccessExpression = function (node) {
        this.checkAndReport(node);
        _super.prototype.visitPropertyAccessExpression.call(this, node);
    };
    NoForeachPushRuleWalker.prototype.checkAndReport = function (node) {
        var isCallExpression = ts.isCallExpression(node.parent);
        var isForEach = node.name.text === 'forEach';
        if (isCallExpression && isForEach) {
            if (this.doesCallPush(node)) {
                this.addFailureAtNode(node.parent, Rule.FAILURE_STRING);
            }
        }
    };
    NoForeachPushRuleWalker.prototype.doesCallPush = function (node) {
        var walker = new PushCallWalker();
        walker.walk(node.parent);
        return walker.isFound;
    };
    return NoForeachPushRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
var PushCallWalker = (function (_super) {
    __extends(PushCallWalker, _super);
    function PushCallWalker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.foundPush = false;
        _this.foundIf = false;
        return _this;
    }
    PushCallWalker.prototype.visitPropertyAccessExpression = function (node) {
        var isCallExpression = ts.isCallExpression(node.parent);
        var isPush = node.name.text === 'push';
        if (isCallExpression && isPush) {
            this.foundPush = true;
            return;
        }
        _super.prototype.visitPropertyAccessExpression.call(this, node);
    };
    PushCallWalker.prototype.visitIfStatement = function () {
        this.foundIf = true;
        return;
    };
    Object.defineProperty(PushCallWalker.prototype, "isFound", {
        get: function () {
            return !this.foundIf && this.foundPush;
        },
        enumerable: true,
        configurable: true
    });
    return PushCallWalker;
}(Lint.SyntaxWalker));
//# sourceMappingURL=noForEachPushRule.js.map