import * as ts from 'typescript';
import * as Lint from 'tslint';
import { ExtendedMetadata } from './utils/ExtendedMetadata';
export declare const FAILURE_STRING: string;
export declare const FAILURE_RECOMMENDATION_STRING: string;
export declare const DEFAULT_MAX_ARGS_LENGTH: number;
export declare class Rule extends Lint.Rules.AbstractRule {
    static metadata: ExtendedMetadata;
    apply(sourceFile: ts.SourceFile): Lint.RuleFailure[];
}
