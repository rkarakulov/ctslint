"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
exports.FAILURE_STRING = 'Exceeds maximum function argument list length of ';
exports.FAILURE_RECOMMENDATION_STRING = '\nConsider these two solutions for refactoring:\n' +
    '1) Create a Class and pass common arguments into the constructor as instance properties. ' +
    'Move this function to the new Class with a reduced arguments list.\n' +
    '2) Instantiate an object containing each of the arguments and pass in the object instance as a single argument.';
exports.DEFAULT_MAX_ARGS_LENGTH = 3;
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new MaxFunctionArgsRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'max-func-args',
        type: 'maintainability',
        description: 'The ideal number of arguments for a function is zero (niladic).',
        options: null,
        optionsDescription: '',
        typescriptOnly: true,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Important',
        level: 'Opportunity for Excellence',
        group: 'Correctness',
        recommendation: '[true, 3],',
        commonWeaknessEnumeration: '',
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var MaxFunctionArgsRuleWalker = (function (_super) {
    __extends(MaxFunctionArgsRuleWalker, _super);
    function MaxFunctionArgsRuleWalker(sourceFile, options) {
        var _this = _super.call(this, sourceFile, options) || this;
        _this.maxArgs = exports.DEFAULT_MAX_ARGS_LENGTH;
        _this.parseOptions();
        return _this;
    }
    MaxFunctionArgsRuleWalker.prototype.visitFunctionDeclaration = function (node) {
        this.checkAndReport(node);
        _super.prototype.visitFunctionDeclaration.call(this, node);
    };
    MaxFunctionArgsRuleWalker.prototype.visitArrowFunction = function (node) {
        this.checkAndReport(node);
        _super.prototype.visitArrowFunction.call(this, node);
    };
    MaxFunctionArgsRuleWalker.prototype.visitMethodDeclaration = function (node) {
        this.checkAndReport(node);
        _super.prototype.visitMethodDeclaration.call(this, node);
    };
    MaxFunctionArgsRuleWalker.prototype.checkAndReport = function (node) {
        if (node.parameters.length > this.maxArgs) {
            var failureMessage = this.makeFailureMessage();
            var _a = this.getStartAndWidth(node.parameters), start = _a.start, width = _a.width;
            this.addFailureAt(start, width, failureMessage);
        }
    };
    MaxFunctionArgsRuleWalker.prototype.getStartAndWidth = function (nodes) {
        var pos = nodes.pos, end = nodes.end;
        var start = pos;
        var width = end - pos;
        return {
            start: start,
            width: width,
        };
    };
    MaxFunctionArgsRuleWalker.prototype.makeFailureMessage = function () {
        return exports.FAILURE_STRING + this.maxArgs + exports.FAILURE_RECOMMENDATION_STRING;
    };
    MaxFunctionArgsRuleWalker.prototype.parseOptions = function () {
        var _this = this;
        this.getOptions().forEach(function (opt) {
            if (typeof opt === 'boolean') {
                return;
            }
            if (typeof opt === 'number') {
                _this.maxArgs = opt;
                return;
            }
        });
    };
    return MaxFunctionArgsRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
//# sourceMappingURL=maxFuncArgsRule.js.map