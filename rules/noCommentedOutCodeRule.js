"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
var tsutils_1 = require("tsutils");
var FAILURE_STRING = 'No commented out code.';
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoCommentedOutCodeRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'no-commented-out-code',
        type: 'maintainability',
        description: 'Code must not be commented out.',
        options: null,
        optionsDescription: '',
        optionExamples: [],
        typescriptOnly: false,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Low',
        level: 'Opportunity for Excellence',
        group: 'Clarity',
        commonWeaknessEnumeration: '',
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var NoCommentedOutCodeRuleWalker = (function (_super) {
    __extends(NoCommentedOutCodeRuleWalker, _super);
    function NoCommentedOutCodeRuleWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoCommentedOutCodeRuleWalker.prototype.visitSourceFile = function (node) {
        var _this = this;
        tsutils_1.forEachTokenWithTrivia(node, function (text, tokenSyntaxKind, range) {
            if (tokenSyntaxKind === ts.SyntaxKind.SingleLineCommentTrivia || tokenSyntaxKind === ts.SyntaxKind.MultiLineCommentTrivia) {
                _this.scanCommentForCode(range.pos, text.substring(range.pos, range.end));
            }
        });
    };
    NoCommentedOutCodeRuleWalker.prototype.scanCommentForCode = function (startPosition, commentText) {
        if (this.textIsCode(this.cleanComment(commentText))) {
            this.foundSuspiciousComment(startPosition, commentText);
        }
    };
    NoCommentedOutCodeRuleWalker.prototype.textIsCode = function (text) {
        text = text.trim();
        if (this.textIsSingleWord(text)) {
            return false;
        }
        if (this.textIsTslintFlag(text)) {
            return false;
        }
        var sourceFile = ts.createSourceFile('', text, ts.ScriptTarget.ES5, true);
        if (sourceFile) {
            var statements = sourceFile.statements;
            var diagnostics = sourceFile.parseDiagnostics;
            return statements.length > 0 && diagnostics.length === 0;
        }
        return false;
    };
    NoCommentedOutCodeRuleWalker.prototype.textIsSingleWord = function (text) {
        var pattern = new RegExp('^[\\w-]*$');
        return pattern.test(text);
    };
    NoCommentedOutCodeRuleWalker.prototype.textIsTslintFlag = function (text) {
        return text.startsWith('tslint:');
    };
    NoCommentedOutCodeRuleWalker.prototype.cleanComment = function (text) {
        var pattern = /^([^a-zA-Z0-9]+)/;
        var lines = text.split('\n');
        return lines.map(function (line) { return line.replace(pattern, '').trim(); }).join('\n');
    };
    NoCommentedOutCodeRuleWalker.prototype.foundSuspiciousComment = function (startPosition, commentText) {
        this.addFailureAt(startPosition, commentText.length, FAILURE_STRING);
    };
    return NoCommentedOutCodeRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
//# sourceMappingURL=noCommentedOutCodeRule.js.map