"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var ErrorTolerantWalker_1 = require("./utils/ErrorTolerantWalker");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoComplexConditionalsRuleWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'no-complex-conditionals',
        type: 'maintainability',
        description: 'Enforce the maximum complexity of conditional expressions.',
        options: null,
        optionsDescription: '',
        optionExamples: [],
        typescriptOnly: false,
        issueClass: 'Non-SDL',
        issueType: 'Warning',
        severity: 'Moderate',
        level: 'Opportunity for Excellence',
        group: 'Clarity',
        commonWeaknessEnumeration: '',
    };
    Rule.FAILURE_STRING = 'Conditional expression is too complex. ' +
        'Consider moving expression to a variable or function with a meaningful name.';
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var NoComplexConditionalsRuleWalker = (function (_super) {
    __extends(NoComplexConditionalsRuleWalker, _super);
    function NoComplexConditionalsRuleWalker(sourceFile, options) {
        var _this = _super.call(this, sourceFile, options) || this;
        _this.threshold = 3;
        _this.parseOptions();
        return _this;
    }
    NoComplexConditionalsRuleWalker.prototype.visitIfStatement = function (node) {
        var expression = node.expression;
        var complexity = this.countComplexity(expression);
        if (complexity > this.threshold) {
            this.addFailureAtNode(expression, Rule.FAILURE_STRING);
        }
        _super.prototype.visitIfStatement.call(this, node);
    };
    NoComplexConditionalsRuleWalker.prototype.countComplexity = function (expression) {
        var _this = this;
        var complexity = 0;
        var cb = function (node) {
            if (_this.increasesComplexity(node)) {
                complexity = complexity + 1;
            }
            return ts.forEachChild(node, cb);
        };
        cb(expression);
        return complexity;
    };
    NoComplexConditionalsRuleWalker.prototype.increasesComplexity = function (node) {
        switch (node.kind) {
            case ts.SyntaxKind.CaseClause:
                return node.statements.length > 0;
            case ts.SyntaxKind.CatchClause:
            case ts.SyntaxKind.ConditionalExpression:
            case ts.SyntaxKind.DoStatement:
            case ts.SyntaxKind.ForStatement:
            case ts.SyntaxKind.ForInStatement:
            case ts.SyntaxKind.ForOfStatement:
            case ts.SyntaxKind.IfStatement:
            case ts.SyntaxKind.WhileStatement:
                return true;
            case ts.SyntaxKind.BinaryExpression:
                switch (node.operatorToken.kind) {
                    case ts.SyntaxKind.BarBarToken:
                    case ts.SyntaxKind.AmpersandAmpersandToken:
                        return true;
                    default:
                        return false;
                }
            default:
                return false;
        }
    };
    NoComplexConditionalsRuleWalker.prototype.parseOptions = function () {
        var _this = this;
        this.getOptions().forEach(function (opt) {
            if (typeof opt === 'boolean') {
                return;
            }
            if (typeof opt === 'number') {
                _this.threshold = opt;
                return;
            }
        });
    };
    return NoComplexConditionalsRuleWalker;
}(ErrorTolerantWalker_1.ErrorTolerantWalker));
//# sourceMappingURL=noComplexConditionalsRule.js.map