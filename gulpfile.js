const gulp = require('gulp');
const del = require('del');

gulp.task('copy-react', () => {
	return gulp
		.src([
			'./node_modules/tslint-react/tslint-react.json',
		])
		.pipe(gulp.dest('./'));
});

gulp.task('clean-clean-code', function () {
	return del('rules', { force: true });
});

gulp.task('copy-clean-code-rules', () => {
	return gulp
		.src([
			'./node_modules/tslint-clean-code/dist/src/**/*',
			'!./node_modules/tslint-clean-code/dist/src/tests/**/*',
			'!./node_modules/tslint-clean-code/dist/src/**/*.map',
		])
		.pipe(gulp.dest('rules'));
});

gulp.task('copy-react-rules', () => {
	return gulp
		.src([
			'./node_modules/tslint-react/rules/**/*',
		])
		.pipe(gulp.dest('rules'));
});

gulp.task('copy-react-utils', () => {
	return gulp
		.src([
			'./node_modules/tslint-react/utils.js',
		])
		.pipe(gulp.dest('./'));
});

gulp.task('default',
	[
		'copy-react',
		'clean-clean-code',
		'copy-clean-code-rules',
		'copy-react-rules',
		'copy-react-utils'
	]);
